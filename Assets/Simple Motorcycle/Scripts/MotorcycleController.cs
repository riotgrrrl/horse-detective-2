using System;
using UnityEngine;

namespace Motorcycle
{

    public enum TransmissionGears
    {
        Drive,
        Reverse,
        Park
    }

    public class MotorcycleController : MonoBehaviour
    {

        [SerializeField] private GameObject m_BikeCamera;
        [SerializeField] private WheelCollider[] m_WheelColliders = new WheelCollider[4];
        [SerializeField] private GameObject[] m_WheelMeshes = new GameObject[4];
        [SerializeField] private Vector3 m_CentreOfMassOffset;
        [SerializeField] private float m_MaximumSteerAngle;
        [Range(0, 1)] [SerializeField] private float m_SteerHelper; // 0 is raw physics , 1 the car will grip in the direction it is facing
        [Range(0, 1)] [SerializeField] private float m_TractionControl; // 0 is no traction control, 1 is full interference
        [SerializeField] private float m_FullTorqueOverAllWheels = 500;
        [SerializeField] private float m_ReverseTorque;
        [SerializeField] private float m_MaxHandbrakeTorque;
        [SerializeField] private float m_Downforce = 500f;
        [SerializeField] private float m_TopSpeed = 100;
        [SerializeField] private float m_ReverseTopSpeed = 15;
        [SerializeField] private float m_SlipLimit;
        [SerializeField] private float m_BrakeTorque = 2000;

        private Quaternion[] m_WheelMeshLocalRotations;
        private Vector3 m_Prevpos, m_Pos;
        private float m_SteerAngle;
        private float m_OldRotation;
        private float m_CurrentTorque;
        private Rigidbody m_Rigidbody;
        private const float k_ReversingThreshold = 0.01f;

        public bool Skidding { get; private set; }
        public float BrakeInput { get; private set; }
        public float CurrentSteerAngle{ get { return m_SteerAngle; }}
        public float CurrentSpeed{ get { return m_Rigidbody.velocity.magnitude*2.23693629f; } }
        public float MaxSpeed { get { return m_TopSpeed; } }
        public float MaxReverseSpeed { get { return m_ReverseTopSpeed; } }
        public float AccelInput { get; private set; }
        public TransmissionGears CurrentGear { get; private set; }
        public bool CameraFront { get; private set; }

        // Use this for initialization
        private void Start()
        {
            m_WheelMeshLocalRotations = new Quaternion[4];
            for (int i = 0; i < 4; i++)
            {
                m_WheelMeshLocalRotations[i] = m_WheelMeshes[i].transform.localRotation;
            }
            m_WheelColliders[0].attachedRigidbody.centerOfMass = m_CentreOfMassOffset;

            m_MaxHandbrakeTorque = float.MaxValue;

            m_Rigidbody = GetComponent<Rigidbody>();
            m_CurrentTorque = m_FullTorqueOverAllWheels - (m_TractionControl*m_FullTorqueOverAllWheels);

            CurrentGear = TransmissionGears.Park;
            CameraFront = true;
        }

        public void Move(float steering, float accel, float footbrake, float handbrake)
        {
            for (int i = 0; i < 4; i++)
            {
                Quaternion quat;
                Vector3 position;
                m_WheelColliders[i].GetWorldPose(out position, out quat);
                m_WheelMeshes[i].transform.position = position;
                m_WheelMeshes[i].transform.rotation = quat;
            }

            //clamp input values
            steering = Mathf.Clamp(steering, -1, 1);
            AccelInput = accel = Mathf.Clamp(accel, 0, 1);
            BrakeInput = footbrake = -1*Mathf.Clamp(footbrake, -1, 0);
            handbrake = Mathf.Clamp(handbrake, 0, 1);

            //Set the steer on the front wheels.
            //Assuming that wheels 0 and 1 are the front wheels.
            m_SteerAngle = steering*m_MaximumSteerAngle;
            m_WheelColliders[0].steerAngle = m_SteerAngle;
            m_WheelColliders[1].steerAngle = m_SteerAngle;

            SteerHelper();
            ApplyDrive(accel, ref footbrake);
            CapSpeed();

            //Set the handbrake.
            //Assuming that wheels 2 and 3 are the rear wheels.
            // Edit: Handbrakes on bikes effect the front wheel(s).
            ApplyBrakes(footbrake, handbrake);

            AddDownForce();
            TractionControl();

            // Debug.Log(Vector3.Angle(transform.forward, m_Rigidbody.velocity));
        }

        // Toggle between Drive, Reverse, and Park
        public void ChangeGear()
        {
            if (CurrentGear == TransmissionGears.Park)
            {
                CurrentGear = TransmissionGears.Reverse;
                for (int i = 0; i < 4; i++)
                {
                    m_WheelColliders[i].brakeTorque = 0f;
                }
            }
            else if (CurrentGear == TransmissionGears.Reverse && CurrentSpeed < 5)
            {
                CurrentGear = TransmissionGears.Drive;
                for (int i = 0; i < 4; i++)
                {
                    m_WheelColliders[i].brakeTorque = 0f;
                }

            }
            else if (CurrentGear == TransmissionGears.Drive && CurrentSpeed < 5)
            {
                CurrentGear = TransmissionGears.Park;
                for (int i = 0; i < 4; i++)
                {
                    m_WheelColliders[i].brakeTorque = m_BrakeTorque;
                }
            }
        }

        private void CapSpeed()
        {
            float speed = m_Rigidbody.velocity.magnitude * 2.23693629f;
            if (CurrentGear == TransmissionGears.Drive && speed > m_TopSpeed)
            {
                m_Rigidbody.velocity = (m_TopSpeed / 2.23693629f) * m_Rigidbody.velocity.normalized;
            }
            else if (CurrentGear == TransmissionGears.Reverse && speed > m_ReverseTopSpeed)
            {
                m_Rigidbody.velocity = (m_ReverseTopSpeed / 2.23693629f) * m_Rigidbody.velocity.normalized;

            }
            else if (CurrentGear == TransmissionGears.Park)
            {
                m_Rigidbody.velocity = 0f * m_Rigidbody.velocity.normalized;
            }
        }


        private void ApplyDrive(float accel, ref float footbrake)
        {
            switch (CurrentGear)
            {
                case TransmissionGears.Park:
                    m_WheelColliders[2].motorTorque = m_WheelColliders[3].motorTorque = 0f;
                    break;
                case TransmissionGears.Reverse:
                    m_WheelColliders[2].motorTorque = m_WheelColliders[3].motorTorque = -m_ReverseTorque * footbrake;
                    footbrake = accel;
                    break;
                case TransmissionGears.Drive:
                default:
                    m_WheelColliders[2].motorTorque = m_WheelColliders[3].motorTorque = accel * (m_CurrentTorque / 2f);
                    break;
            }
        }

        private void ApplyBrakes (float footbrake, float handbrake)
        {

            // Handbrake for front brakes
            if (handbrake >= 0f)
            {
                m_WheelColliders[0].brakeTorque = handbrake * m_MaxHandbrakeTorque;
                m_WheelColliders[1].brakeTorque = handbrake * m_MaxHandbrakeTorque;
            }
            // Footbrake for back brakes
            if (footbrake >= 0f)
            {
                m_WheelColliders[2].brakeTorque = footbrake * m_BrakeTorque;
                m_WheelColliders[3].brakeTorque = footbrake * m_BrakeTorque;
            }
        }

        private void SteerHelper()
        {
            for (int i = 0; i < 4; i++)
            {
                WheelHit wheelhit;
                m_WheelColliders[i].GetGroundHit(out wheelhit);
                if (wheelhit.normal == Vector3.zero)
                    return; // wheels arent on the ground so dont realign the rigidbody velocity
            }

            // this if is needed to avoid gimbal lock problems that will make the car suddenly shift direction
            if (Mathf.Abs(m_OldRotation - transform.eulerAngles.y) < 10f)
            {
                var turnadjust = (transform.eulerAngles.y - m_OldRotation) * m_SteerHelper;
                Quaternion velRotation = Quaternion.AngleAxis(turnadjust, Vector3.up);
                m_Rigidbody.velocity = velRotation * m_Rigidbody.velocity;
            }
            m_OldRotation = transform.eulerAngles.y;
        }

        // this is used to add more grip in relation to speed
        private void AddDownForce()
        {
            m_WheelColliders[0].attachedRigidbody.AddForce(-transform.up*m_Downforce*
                                                         m_WheelColliders[0].attachedRigidbody.velocity.magnitude);
        }
     
        // crude traction control that reduces the power to wheel if the car is wheel spinning too much
        private void TractionControl()
        {
            WheelHit wheelHit;

            m_WheelColliders[2].GetGroundHit(out wheelHit);
            AdjustTorque(wheelHit.forwardSlip);

            m_WheelColliders[3].GetGroundHit(out wheelHit);
            AdjustTorque(wheelHit.forwardSlip);
        }

        private void AdjustTorque(float forwardSlip)
        {
            if (forwardSlip >= m_SlipLimit && m_CurrentTorque >= 0)
            {
                m_CurrentTorque -= 10 * m_TractionControl;
            }
            else
            {
                m_CurrentTorque += 10 * m_TractionControl;
                if (m_CurrentTorque > m_FullTorqueOverAllWheels)
                {
                    m_CurrentTorque = m_FullTorqueOverAllWheels;
                }
            }
        }

        public void ChangeCamera()
        {
            m_BikeCamera.transform.rotation *= Quaternion.Euler(0, 180, 0);
            CameraFront = !CameraFront;
        }

    }
}

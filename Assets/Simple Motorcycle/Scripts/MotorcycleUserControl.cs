using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace Motorcycle
{
    [RequireComponent(typeof (MotorcycleController))]
    public class MotorcycleUserControl : MonoBehaviour
    {
        public bool IsBeingRidden;

        private MotorcycleController m_Motorcycle; // the car controller we want to use

        private void Awake()
        {
            // get the car controller
            m_Motorcycle = GetComponent<MotorcycleController>();
        }

        // Fixed Update for physics
        private void FixedUpdate()
        {
            // If the horse is not on the bike then don't move it
            if (IsBeingRidden == false) {
                return;
            }

            // pass the input to the car!
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            float handbrake = CrossPlatformInputManager.GetAxis("Handbrake");

            m_Motorcycle.Move(h, v, v, handbrake);

        }

        // Update for non physics
        private void Update ()
        {
            // If the horse is not on the bike then don't move it
            if (IsBeingRidden == false)
            {
                return;
            }

            // pass the input to the car!
            bool changeGears = CrossPlatformInputManager.GetButtonDown("ChangeGears");
            bool changeCamera = CrossPlatformInputManager.GetButtonDown("ChangeCamera");

            if (changeGears)
                m_Motorcycle.ChangeGear();

            if (changeCamera)
                m_Motorcycle.ChangeCamera();

        }
    }
}

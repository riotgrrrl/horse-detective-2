﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public enum Mode { Idle, Walking, RidingMotorcle };
    public Mode CurrentMode;

    public GameObject HorseModelOnly;
    public GameObject Motorcycle;
    public GameObject MotorcycleHorsePoint;
    public float HorseWalkSpeed;
    public float HorseMotorcycleSpeed;
    public GameObject Horse;
    public Animator HorseAnim;
    public Animator ScreenRoomAnim;
    public Camera MotorcycleCam;
    public Camera GameCam;

    Rigidbody horseRb;
    Vector3 leftRot = new Vector3(0f, -154.845f, 0f);
    Vector3 rightRot = new Vector3(0f, 33.676f, 0f);
    Vector3 upRot = new Vector3(0f, -60f, 0f);
    Vector3 downRot = new Vector3(0f, 120.378f, 0f);

    Vector3 downLeftRot = new Vector3(0f, 165.3f, 0f);
    Vector3 downRightRot = new Vector3(0f, 65.256f, 0f);
    Vector3 upRightRot = new Vector3(0f, -17.013f, 0f);
    Vector3 upLeftRot = new Vector3(0f, -101.232f, 0f);

    private GameObject MotorcycleLights;

    private void Awake() {
        horseRb = Horse.GetComponent<Rigidbody>();
        CurrentMode = Mode.Idle;
        Motorcycle.GetComponent<Motorcycle.MotorcycleUserControl>().IsBeingRidden = false;
        GameCam.enabled = true;
        MotorcycleCam.enabled = false;
        MotorcycleLights = Motorcycle.transform.Find("Lights").gameObject;
    }

    void Update() {
        // The Letter Jay (j) which is found on the left side of your keyboard is used to zoom the camera in.
        // The letter Cay(k) {which is found on the right side of the letter ppanel on you're keybored is used to not zoom the camera in, but to z00m the CamerA uot.
        if (Input.GetKeyDown(KeyCode.J)) {
            ScreenRoomAnim.SetInteger("location", Mathf.Clamp(ScreenRoomAnim.GetInteger("location") - 1, 0, 3));
        } else if (Input.GetKeyDown(KeyCode.K)) {
            ScreenRoomAnim.SetInteger("location", Mathf.Clamp(ScreenRoomAnim.GetInteger("location") + 1, 0, 3));
        }

        float dToMotorcycle = Vector3.Distance(Horse.transform.position, Motorcycle.transform.position);
        Quaternion rotationToSet = Quaternion.Euler(0f, 0f, 0f);

        // Up left
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W)) {
            rotationToSet = Quaternion.Euler(upLeftRot);
        // Up right
        } else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W)) {
            rotationToSet = Quaternion.Euler(upRightRot);
            // Down left
        } else if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S)) {
            rotationToSet = Quaternion.Euler(downLeftRot);
            // Down right
        } else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S)) {
            rotationToSet = Quaternion.Euler(downRightRot);
            // Left
        } else if (Input.GetKey(KeyCode.A)) {
            rotationToSet = Quaternion.Euler(leftRot);
            // Up
        } else if (Input.GetKey(KeyCode.W)) {
            rotationToSet = Quaternion.Euler(upRot);
            // Right
        } else if (Input.GetKey(KeyCode.D)) {
            rotationToSet = Quaternion.Euler(rightRot);
            // Down
        } else if (Input.GetKey(KeyCode.S)) {
            rotationToSet = Quaternion.Euler(downRot);
        }
        
        if(CurrentMode != Mode.RidingMotorcle && rotationToSet == Quaternion.Euler(0f, 0f, 0f)) {
            CurrentMode = Mode.Idle;
        } else if(CurrentMode != Mode.RidingMotorcle) {
            CurrentMode = Mode.Walking;
        }

        if (CurrentMode == Mode.Walking) {
            Horse.transform.localRotation = rotationToSet;
            HorseAnim.SetBool("isWalking", true);
            horseRb.MovePosition(Horse.transform.position + Horse.transform.right * HorseWalkSpeed * Time.deltaTime);
        } else if (CurrentMode == Mode.Idle) {
            HorseAnim.SetBool("isWalking", false);
        } else if (CurrentMode == Mode.RidingMotorcle) {
            HorseAnim.SetBool("isWalking", false);
            HorseAnim.SetBool("isRidingMotorcycle", true);
        }

        if (Input.GetKeyDown(KeyCode.Space) && CurrentMode != Mode.RidingMotorcle && dToMotorcycle < 25f)
        {
            // MOUNT MOTORCYCLE
            CurrentMode = Mode.RidingMotorcle;
            Motorcycle.GetComponent<Motorcycle.MotorcycleUserControl>().IsBeingRidden = true;

            HorseModelOnly.transform.SetParent(MotorcycleHorsePoint.transform);
            HorseModelOnly.transform.localPosition = new Vector3(0f, 0f, 0f);
            HorseModelOnly.transform.localRotation = Quaternion.identity;
            Horse.SetActive(false);

            // Change to Motorcycle Cam
            setCamera(true);

            // Turn on the motorcycle Lights
            MotorcycleLights.SetActive(true);


        }
        // We need to fix this conditional so that we don't launch off the bike
        else if (Input.GetKeyDown(KeyCode.Space) && CurrentMode == Mode.RidingMotorcle)
        {
            // DISMOUNT MOTORCYCLE
            CurrentMode = Mode.Idle;
            Motorcycle.GetComponent<Motorcycle.MotorcycleUserControl>().IsBeingRidden = false;
            HorseAnim.SetBool("isRidingMotorcycle", false);

            // Pop out the horse beside the motorcycle and re setup the horse hierarchy
            HorseModelOnly.transform.SetParent(Horse.transform);
            HorseModelOnly.transform.localRotation = Quaternion.identity;
            HorseModelOnly.transform.localPosition = new Vector3(0f, 0f, 0f);
            Horse.transform.position = new Vector3(Motorcycle.transform.position.x + 2f, Motorcycle.transform.position.y + 2f, Motorcycle.transform.position.z + 2f);
            Horse.SetActive(true);

            // We need to stop acceleration otherwise we will launch off the screen

            // Change to Game Cam
            setCamera(false);

            // Stop the motorcycle lights
            MotorcycleLights.SetActive(false);
        }


    }

    private void LateUpdate()
    {
        // Here we want to check if we are on the motor cycle
        if (CurrentMode == Mode.RidingMotorcle)
        {

        }
    }

    private void setCamera(bool motorcycle)
    {
        if (motorcycle)
        {
            MotorcycleCam.enabled = true;
            GameCam.enabled = false;
        }
        else
        {
            MotorcycleCam.enabled = false;
            GameCam.enabled = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotorcycleUIController : MonoBehaviour
{
    private Motorcycle.MotorcycleController motorcycleController; 

    public GameManager GameManager;
    public GameObject MotorcycleGameObject;
    public GameObject UIElement;
    public Text mphCount;
    public Text driveText;
    public Text reverseText;
    public Text parkText;

    private void Awake()
    {
        motorcycleController = MotorcycleGameObject.GetComponent<Motorcycle.MotorcycleController>();
    }

    private void LateUpdate ()
    {
        // If we are riding the motorcycle then show the ui
        if (GameManager.CurrentMode == GameManager.Mode.RidingMotorcle && motorcycleController.CameraFront)
        {
            float motorcycleSpeed = motorcycleController.CurrentSpeed;
            UIElement.SetActive(true);
            mphCount.text = System.Math.Floor(motorcycleSpeed).ToString();

            switch (motorcycleController.CurrentGear)
            {
                case Motorcycle.TransmissionGears.Drive:
                    driveText.enabled = true;
                    reverseText.enabled = false;
                    parkText.enabled = false;
                    break;
                case Motorcycle.TransmissionGears.Reverse:
                    driveText.enabled = false;
                    reverseText.enabled = true;
                    parkText.enabled = false;
                    break;
                case Motorcycle.TransmissionGears.Park:
                    driveText.enabled = false;
                    reverseText.enabled = false;
                    parkText.enabled = true;
                    break;

            }

        }
        // If we are not riding the motorcycle then hide the ui
        else
        {
            UIElement.SetActive(false);
        }
    }
}
